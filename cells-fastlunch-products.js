{
  const {
    html,
  } = Polymer;
  /**
    `<cells-fastlunch-products>` Description.

    Example:

    ```html
    <cells-fastlunch-products></cells-fastlunch-products>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-fastlunch-products | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsFastlunchProducts extends Polymer.Element {

    static get is() {
      return 'cells-fastlunch-products';
    }

    static get properties() {
      return {
        isOrdering: {
          type: Boolean,
          value: false,
          notify: true
        },
        productsType: {
          type: Array
        },
        products: {
          type: Array,
          value: []
        },
        productList: {
          type: Array,
          notify: true
        },
        productsSelected: {
          type: Array,
          value: [],
          notify: true
        },
        otherType: Object,
        typeSelected: Object

      };
    }

    _selectProductType(e) {
      let item = this.shadowRoot.querySelectorAll('#cardItemList')[0].itemForElement(e.target);
      this.isOrdering = true;
      this.setOrderStatus(true);
      this.typeSelected = item;
      this.otherType = this.productsType.find(itemFind => itemFind.typeId != item.typeId);
      this._setProductFormat();
    }

    setOrderStatus(value){
      this.dispatchEvent(new CustomEvent('order-status', {
        bubbles: true,
        composed: true,
        detail: value
      })); 
    }

    _setProductFormat() {
      this.productList = [];
      if (localStorage.getItem('fastlunch-productsSelected') != "" ){
        let prodSelected = JSON.parse(localStorage.getItem('fastlunch-productsSelected'));
        if (prodSelected != undefined) this.productsSelected = prodSelected;
      }      
      this.products.forEach((item, index) => {
        if (this.typeSelected && item.typeId != this.typeSelected.typeId) return;
        let productFormat = {
          productId: item.idProducto,
          imgSrc: item.nombreImg,
          name: item.nombre,
          description: {
            masked: false,
            content: item.descripcion
          },
          count: 0,
          primaryAmount: {
            amount: item.precio,
            currency: 'PEN'
          },
          stock: item.stock,
        }
        let itemSelected = this.productsSelected.findIndex(itemFiltered=> itemFiltered.productId == item.idProducto);
        if (itemSelected > -1) productFormat.count = itemSelected.count;
        this.productList.push(productFormat); 
      });      
      Polymer.RenderStatus.afterNextRender(this, () => this.obtainProductsSelected());
    }

    onClickProductAction(event) {
      let rowDiv = event.currentTarget;
      let productIdSelected = rowDiv.getAttribute('data-productId');
      let indexData = rowDiv.getAttribute('data-index');
      let typeAction = rowDiv.getAttribute('data-typeAction') == "add" ? true : false;
      let itemSelected = this.productList.find(item => item.productId == productIdSelected);
      if (typeAction && (itemSelected.count >= itemSelected.stock)){
        this._showMaximumMessage({detail: itemSelected.stock});
        return false;
      }

      let temp = this.productList.map((item, index) => {
        if (item.productId === itemSelected.productId) {
         item = this.setButtonClass(item, indexData, typeAction);
        }
        return item;
      });
      this.productList = [];
      this.productList = temp;
      this.notifyPath('productList');
      this.obtainProductsSelected();
      return true;
    }

    obtainProductsSelected(){
      //this.productsSelected = [];
      //let productsSelected_= [];
      this.productList.forEach((item) =>{
        if (item.count > 0){
          let indexProd
          if (this.productsSelected){
            indexProd = this.productsSelected.findIndex(itemFiltered => itemFiltered.productId == item.productId);
          } else {
            this.productsSelected = [];
          }
          if (indexProd == undefined || indexProd == -1){
            this.productsSelected.push(item);
          } else {
            this.productsSelected[indexProd] = item;
          }
          
        }
      });
      let productSelectedValid =[];
      this.productsSelected.forEach(item => {
        if (item.count > 0){
          productSelectedValid.push(item);
        }
      })
      this.productsSelected = productSelectedValid;
      this.notifyPath('productsSelected');
      let prodSummary = this.shadowRoot.querySelector('#productsSummary');
      prodSummary.items = this.productsSelected;
      prodSummary.init();
      localStorage.setItem('fastlunch-productsSelected',JSON.stringify(this.productsSelected));
    }

    setButtonClass(item, indexData, action) {
      if (action) {
        item.count += 1;
      } else {
        item.count -= 1;
      }
      if ((action && item.count ==1) || (!action && item.count ==0)){
        this.shadowRoot.getElementById(indexData + '_button').classList.toggle("show");
        this.shadowRoot.getElementById(indexData + '_button-count').classList.toggle("show");
      }
      if (item.count > 0) {
        this.shadowRoot.getElementById(indexData + '_count').innerHTML = item.count;
      }
      return item;
    }

    _cleanProductsList(evt){
      let productList_ = this.productList.map((item, index) => {
        if (item.count > 0) {
          this.shadowRoot.getElementById(index+'_count').innerHTML = 0;
          item.count = 0;
          this.shadowRoot.getElementById(index + '_button').classList.toggle("show");
          this.shadowRoot.getElementById(index + '_button-count').classList.toggle("show");
        }
        return item;
      });
      this.set('productsSelected',[]);
      this.set('productList',productList_);
    }

    _showCollapsibleList() {
      this.dispatchEvent(new CustomEvent('show-collapsible-list', {}));
    }
    _hideCollapsibleList() {
      this.dispatchEvent(new CustomEvent('hide-collapsible-list', {}));
    }

    _payProductsList(evt){
      this.dispatchEvent(new CustomEvent('pay-products', {
        bubbles: true,
        composed: true,
        detail: this.productsSelected
      }));    
    }

    _updateList(evt){
      let productsUpdated = evt.detail;

      this.productList.forEach((item, index) => {
        let product = productsUpdated.find(itemUpd => itemUpd.productId == item.productId);
        if (!product) {
          item.count = 0;
          this.shadowRoot.getElementById(index+'_count').innerHTML = 0;
          this.shadowRoot.getElementById(index + '_button').classList.add("show");
          this.shadowRoot.getElementById(index + '_button-count').classList.remove("show");
          return;
        }
        item.count = product.count;
        this.shadowRoot.getElementById(index+'_count').innerHTML = item.count;
        if (item.count ==0){
          this.shadowRoot.getElementById(index + '_button').classList.add("show");
          this.shadowRoot.getElementById(index + '_button-count').classList.remove("show");
        } else if (item.count >=1){
          this.shadowRoot.getElementById(index + '_button').classList.remove("show");
          this.shadowRoot.getElementById(index + '_button-count').classList.add("show");
        }
      });
    }

    changeType(){
      this.set("typeSelected",this.otherType);
      this.otherType = this.productsType.find(itemFind => itemFind.typeId != this.typeSelected.typeId);      
      this._setProductFormat();
    }

    _showMaximumMessage(message){
      this.dispatchEvent(new CustomEvent('show-maximum-alert', {detail: "Solo quedan " + message.detail +" unidades"}));
    }

  }

  customElements.define(CellsFastlunchProducts.is, CellsFastlunchProducts);
}